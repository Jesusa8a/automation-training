*** Keywords ***
Open Browser To Login Page
    Open Browser                https://www.${siteURL}  Chrome
    Maximize Browser Window
    Wait And Focus On Element   //a[@id="uh-mail-link"]
    Click Link                  //a[@id="uh-mail-link"]

Input User
    [Arguments]                 ${user}=${userName}
    Wait And Focus On Element   //form[@id="login-username-form"]
    Input Text                  //input[@name="username"]   ${user}
    Next Button

Input Password
    [Arguments]                 ${pass}=${password}
    Wait And Focus On Element   //input[@name="password"]
    Input Text                  //input[@name="password"]   ${pass}
    Submit Button

Input User And Password
    [Arguments]             ${user}=${userName}     ${pass}=${password}
    Input User              ${user}
    Input Password          ${pass}

Next Button
    Wait And Focus On Element   //input[@id="login-signin"]
    Press Key                   //input[@id="login-signin"]     \\9
    Press Key                   //input[@id="login-signin"]     \\13

Submit Button
    Wait And Focus On Element   //button[@id="login-signin"]
    Press Key                   //button[@id="login-signin"]     \\9
    Press Key                   //button[@id="login-signin"]     \\13

Wait And Focus On Element
    [Arguments]             ${locator}      ${timeout}=15s
    Wait Until Element Is Visible       ${locator}
    Set Focus To Element                ${locator}
