*** Settings ***

Documentation       A test suite with a single test for valid login.
...                 This test has a workflow that is created using keywords in
...                 the imported resource file.

Resource    ../resource.robot

*** Test Cases ***

Valid Login
    [Setup]         Open Browser To Login Page
    [Teardown]      Close Browser
    Input User And Password     ${userName}     ${password}
    Sign In Page Loads
